/*============================== 
	- MAP JS (ICON MARKER)
	- Template: MARRY - Responsive HTML Wedding Template
	- Author: DoubleEight
	- Version: 1.0.0
	- Website: themeforest.net/user/doubleeight/portfolio
================================= */
	
(function($) {

'use strict';		
	
	// CHECK WINDOW RESIZE
	var is_windowresize = false;
	$(window).resize(function(){
		is_windowresize = true;
	});
	
	
	//INITIALIZE MAP
	function initialize() {
		
		// Create an array of styles
		//=======================================================================================
  		var styles = [
    		{
      			stylers: [
        			{ hue: "#f98d8a" },
        			{ saturation: -50 }
      			]
    		}
  		];
		
		// Create a new StyledMapType object, passing it the array of styles,
  		// as well as the name to be displayed on the map type control.
  		var styledMap = new google.maps.StyledMapType(styles,
   			{name: "Styled Map"});
			
		
		//DEFINE MAP OPTIONS
		//=======================================================================================
  		var mapOptions = {
    		zoom: 16,		
			mapTypeId: google.maps.MapTypeId.ROADMAP,	
    		center: new google.maps.LatLng(23.870990, 120.923485),
			panControl: true,
  			zoomControl: true,
  			mapTypeControl: true,
  			//scaleControl: false,
  			streetViewControl: true,
  			overviewMapControl: true,
			//rotateControl:true,			
			scrollwheel: false,
  		};

		//CREATE NEW MAP
		//=======================================================================================
  		var map = new google.maps.Map(document.getElementById('map-canvas-3'), mapOptions);
		
		//Associate the styled map with the MapTypeId and set it to display.
 		 map.mapTypes.set('map_style', styledMap);
 		 map.setMapTypeId('map_style');
		
		//MARKER ICON
		//=======================================================================================
		//var image = 'facebook30.svg';
		
		//ADD NEW MARKER
		//=======================================================================================
  		/*var marker = new google.maps.Marker({
    		position: map.getCenter(),
   		 	map: map,
    		title: 'Click to zoom',
			icon: image
  		});
		
		var marker1 = new google.maps.Marker({
    		position: new google.maps.LatLng(35.441938, -97.432494),
   		 	map: map,
    		title: 'Click to zoom'
  		});*/
		
		
		//ADD NEW MARKER WITH LABEL
		//=======================================================================================
		var marker1 = new MarkerWithLabel({
       		position: new google.maps.LatLng(23.870990, 120.923485),
       		draggable: false,
       		raiseOnDrag: false,
       		icon: ' ',
       		map: map, 
         	labelContent: '<div id="wedding-marker" class="main-icon-wrapper"><div class="big-circle scale-animation"></div><div class="main-icon-text">Wedding</br>Location</div></div>',
       		labelAnchor: new google.maps.Point(50, 50),
       		labelClass: "labels" // the CSS class for the label
     	});
		
		
		//INFO WINDOWS 1
		//=======================================================================================
		var contentString1 = ''+
		'<div class="info-window-wrapper">'+
			'<h6>CEREMONY & RECEPTION</h6>'+
			'<div class="info-window-desc">Hotel location. Get Google map directions.</div>'+
			'<div class="info-window-link"><a href="https://goo.gl/maps/2KqL9XERehP2" target="_blank" class="grey-link with-underline">Click Here</a></div>'+
      	'</div>';
		
		var marker1_infowindow = new google.maps.InfoWindow({
      		content: contentString1,
			maxWidth: 200,
			pixelOffset: new google.maps.Size(0,-10)
  		});
		
		//OPEN INFO WINDOWS ON LOAD
  		marker1_infowindow.open(map,marker1);
		
		//ON MARKER CLICK EVENTS
		google.maps.event.addListener(marker1, 'click', function() {
			marker1_infowindow.open(map,marker1);	
  		});
	





		var map2Options = {
    		zoom: 13,		
			mapTypeId: google.maps.MapTypeId.ROADMAP,	
    		center: new google.maps.LatLng(23.895923, 120.920335),
			panControl: true,
  			zoomControl: true,
  			mapTypeControl: true,
  			//scaleControl: false,
  			streetViewControl: true,
  			overviewMapControl: true,
			//rotateControl:true,			
			scrollwheel: false,
  		};


		  //CREATE NEW MAP
		//=======================================================================================
		var map2 = new google.maps.Map(document.getElementById('map-canvas-3b'), map2Options);
		
		//Associate the styled map with the MapTypeId and set it to display.
 		 map2.mapTypes.set('map_style', styledMap);
 		 map2.setMapTypeId('map_style');
		
		  //INFO WINDOWS 1
		//=======================================================================================
		var contentStringBBQ = ''+
		'<div class="info-window-wrapper">'+
			'<h6>SUNDAY BBQ</h6>'+
			'<div class="info-window-desc">Black Tea Workshop B & B. Get Google map directions.</div>'+
			'<div class="info-window-link"><a href="https://goo.gl/maps/M6g6pCfMFay" target="_blank" class="grey-link with-underline">Click Here</a></div>'+
		  '</div>';
		  
		  //ADD NEW MARKER WITH LABEL
		//=======================================================================================
		var markerBBQ = new MarkerWithLabel({
			position: new google.maps.LatLng(23.922707, 120.918904),
			draggable: false,
			raiseOnDrag: false,
			icon: ' ',
			map: map2, 
		  	labelContent: '<div id="wedding-marker" class="main-icon-wrapper"><div class="big-circle scale-animation"></div><div class="main-icon-text">BBQ</br>Location</div></div>',
			labelAnchor: new google.maps.Point(50, 50),
			labelClass: "labels" // the CSS class for the label
	  });
		
		var markerBBQ_infowindow = new google.maps.InfoWindow({
      		content: contentStringBBQ,
			maxWidth: 200,
			pixelOffset: new google.maps.Size(0,-10)
  		});
		
		//OPEN INFO WINDOWS ON LOAD
		markerBBQ_infowindow.open(map2,markerBBQ);
		
		//ON MARKER CLICK EVENTS
		google.maps.event.addListener(markerBBQ, 'click', function() {
			markerBBQ_infowindow.open(map2,markerBBQ);	
		});

		  //INFO WINDOWS 2
		//=======================================================================================
		var contentString2b = ''+
		'<div class="info-window-wrapper">'+
			'<h6>SUNDAY BOAT TRIP</h6>'+
			'<div class="info-window-desc">Shuishe Pier Plaza. Get Google map directions.</div>'+
			'<div class="info-window-link"><a href="https://goo.gl/maps/UAqGovKZzHo" target="_blank" class="grey-link with-underline">Click Here</a></div>'+
		  '</div>';
		  
		  //ADD NEW MARKER WITH LABEL
		//=======================================================================================
		var marker2b = new MarkerWithLabel({
			position: new google.maps.LatLng(23.864499, 120.911826),
			draggable: false,
			raiseOnDrag: false,
			icon: ' ',
			map: map2, 
		  	labelContent: '<div id="wedding-marker" class="main-icon-wrapper"><div class="big-circle scale-animation"></div><div class="main-icon-text">Boat</br>Location</div></div>',
			labelAnchor: new google.maps.Point(50, 50),
			labelClass: "labels" // the CSS class for the label
	  });
		
		var marker2b_infowindow = new google.maps.InfoWindow({
      		content: contentString2b,
			maxWidth: 200,
			pixelOffset: new google.maps.Size(0,-10)
  		});
		
		//OPEN INFO WINDOWS ON LOAD
		marker2b_infowindow.open(map2, marker2b);
		
		//ON MARKER CLICK EVENTS
		google.maps.event.addListener(marker2b, 'click', function() {
			marker2b_infowindow.open(map2, marker2b);	
		});
		  
		  
		
		
		//ON BOUND EVENTS AND WINDOW RESIZE
		//=======================================================================================
		google.maps.event.addListener(map2, 'bounds_changed', function() {  		
			if (is_windowresize)
			{
				//map.setCenter(marker.getPosition());
				window.setTimeout(function() {
					map2.panTo(marker1b.getPosition());
    			}, 500);
			}
			is_windowresize=false;
		});

		
	}

	// LOAD GMAP
	google.maps.event.addDomListener(window, 'load', initialize);
	
	
})(jQuery);