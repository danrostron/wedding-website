<%@LANGUAGE="VBSCRIPT"%>
<%

Function SecureHeaderField(sFieldValue) 
	SecureHeaderField = Replace(Replace(sFieldValue, Chr(10), " "), Chr(13), " ") 
End Function 

 'The header/footer for the email

 Const Header = "Wedding RSVP"
 
 Const Footer = "**********************************END***********************************"

 ' read all the form elements and place them in the vairable mail_body

    Dim mail_Body
    mail_Body = Header & vbCrLf & vbCrLf
    mail_Body = mail_Body & "FORM submitted at " & Now() & vbCrLf & vbCrLf


    'Order the fields as they are on the form
    Dim formItems, itemLabel, itemValue
    formItems = Split(Request.Form("all_input_id[]"), ", ")

    For Each item In formItems
        itemLabel = SecureHeaderField(Request.Form(item & "_label"))
        itemValue = SecureHeaderField(Request.Form(item))

        if Len(itemLabel) > 0 Then
            mail_Body = mail_Body & itemLabel & " : " & itemValue & vbCrLf 
        End If
    Next

    
    mail_Body = mail_Body & vbCrLf & Footer

    'Create the mail object and send the mail
	Set objMail = CreateObject("CDONTS.NewMail")


'Check for the "email" field and if it is empty give the From field something useful!

	objMail.From = "info@danrostron.com"
	objMail.To = "djr@danrostron.com"
	objMail.CC = "youshihyu@gmail.com"
	objMail.Subject = "Wedding RSVP"
	objMail.Body = mail_Body
	objMail.Send()

    Set objMail = Nothing
    
    Response.ContentType = "application/json"
    Response.Write("{ ""type"": ""success"", ""text"": ""RSVP received, thank you!""}")
%>